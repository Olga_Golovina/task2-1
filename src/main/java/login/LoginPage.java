package login;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class LoginPage {
    public WebDriver driver;
    @FindBy(name = "username")
    public static WebElement login;

    @FindBy(name = "password")
    public static WebElement password;

    @FindBy(className = "radius")
    public static WebElement loginClick;

    @FindBy(xpath = "//*[contains(text(),'You logged into a secure area!')]")
    public static WebElement correctLabel;

    @FindBy(xpath = "//*[contains(text(),'Your username is invalid!')]")
    public static WebElement noLoginLabel;

    @FindBy(xpath = "//*[contains(text(),'Your password is invalid!')]")
    public static WebElement noPasswordLabel;

    public LoginPage (WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    //открытие требуемой страницы
    public static void openLoginPage(WebDriver driver) {

        driver.get("http://the-internet.herokuapp.com/login");

    }

    public static void maximizeBrowser(WebDriver driver) {
        driver.manage().window().maximize();
    }

    public static void ClickForElement (WebElement element) {

        element.click();
    }

    public static void checkElementDisplayed(WebElement element) {

        Assert.assertTrue(element.isDisplayed());
    }

    public static void enterField(WebElement element, String value) {
        element.sendKeys(value);
    }
}

