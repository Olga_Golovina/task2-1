import io.github.bonigarcia.wdm.WebDriverManager;
import login.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static login.LoginPage.*;
import static login.LoginPage.noPasswordLabel;

public class loginTest {
    public WebDriver driver;
    private LoginPage newLoginPage;

    @BeforeMethod
    public void setupClass() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        newLoginPage = new LoginPage(driver);
    }

    @Test(description = "Корректный логин", priority = 0)
    public void testCorrect() {

        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем нужную страницу
        openLoginPage(driver);

        //Вводим корректные значения в поля логина и пароля
        enterField(login, "tomsmith");
        enterField(password, "SuperSecretPassword!");

        //кликаем мышью
        ClickForElement(loginClick);

        //ассерт требуемого лейбла
        checkElementDisplayed(correctLabel);


    }

    @Test(description = "Некорректный логин", priority = 1)
    public void testNoCorrectLogin() {

        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем нужную страницу
        openLoginPage(driver);

        //Вводим некорректное значение в поля логина
        enterField(login, "tomsmith111");

        //кликаем мышью
        ClickForElement(loginClick);

        //ассерт требуемого лейбла
        checkElementDisplayed(noLoginLabel);

    }

    @Test(description = "Некорректный пароль", priority = 2)
    public void testNoCorrectPassword() {

        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем нужную страницу
        openLoginPage(driver);

        //Вводим в поля корректные логин и некорректный пароль
        enterField(login, "tomsmith");
        enterField(password, "1111");

        //кликаем мышью
        ClickForElement(loginClick);

        //ассерт требуемого лейбла
        checkElementDisplayed(noPasswordLabel);

    }

    @AfterMethod
    public void teardown() {
        if (driver != null) {
            driver.quit();

        }
    }
}
